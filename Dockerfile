ARG BUILD_DIR=/app
ARG DEPS="ca-certificates ffmpeg icu libressl libffi libmagic py3-pip"
ARG PYTHON_BIN="/usr/bin/python3"
ARG PYTHON_PKG="python3"
ARG APP_USER=bot
ARG APP_GROUP=bot

FROM alpine:3.20.2 AS builder

ARG BUILD_DIR
ENV BUILD_DIR=${BUILD_DIR}

ARG APP_USER
ENV APP_USER=${APP_USER}

ARG APP_GROUP
ENV APP_GROUP=${APP_GROUP}

ARG DEPS
ENV DEPS=${DEPS}

ARG PYTHON_BIN
ENV PYTHON_BIN=${PYTHON_BIN}

ARG PYTHON_PKG
ENV PYTHON_PKG=${PYTHON_PKG}

ENV BUILD_DEPS="build-base icu-dev musl-dev libressl-dev libffi-dev libjpeg-turbo-dev curl poetry zlib-dev"

ARG PYTHON_BIN
ENV PYTHON_BIN=${PYTHON_BIN}

COPY . ${BUILD_DIR}

# hadolint ignore=DL3018
RUN apk --no-cache --update add --virtual build-deps ${BUILD_DEPS} \
    && apk --no-cache --update add ${PYTHON_PKG} ${PYTHON_PKG}-dev ${DEPS} \
    && addgroup -g 5000 ${APP_GROUP} \
    && adduser -S -s /sbin/nologin -u 5000 -G ${APP_GROUP} ${APP_USER} \
    && chown -R "${APP_USER}:${APP_GROUP}" "${BUILD_DIR}"

USER ${APP_USER}

WORKDIR ${BUILD_DIR}

RUN poetry update \
    && poetry build \
    && poetry export -f requirements.txt > requirements.txt \
    && ${PYTHON_BIN} -m pip wheel -w "${BUILD_DIR}"/dist --no-cache-dir -r requirements.txt

FROM alpine:3.20.2

ARG BUILD_DIR
ENV BUILD_DIR=${BUILD_DIR}

ARG APP_USER
ENV APP_USER=${APP_USER}

ARG APP_GROUP
ENV APP_GROUP=${APP_GROUP}

ENV DIST_DIR=${BUILD_DIR}/dist

ARG DEPS
ENV DEPS=${DEPS}

ARG PYTHON_BIN
ENV PYTHON_BIN=${PYTHON_BIN}

ARG PYTHON_PKG
ENV PYTHON_PKG=${PYTHON_PKG}

ENV APP_DIR=/opt
ENV PATH=/home/${APP_USER}/.local/bin:$PATH
ENV LANG=C

COPY --from=builder ${DIST_DIR}/ ${APP_DIR}/
COPY entrypoint.sh /

# hadolint ignore=DL3018
RUN addgroup -g 5000 ${APP_GROUP} \
    && adduser -S -s /sbin/nologin -u 5000 -G ${APP_GROUP} ${APP_USER} \
    && apk --no-cache --update add ${PYTHON_PKG} ${DEPS} git \
    && rm -rf /var/cache/apk/* \
    && chown -R ${APP_USER}:${APP_GROUP} ${APP_DIR}

USER ${APP_USER}

RUN python -m venv ${APP_DIR}/venv \
  && . ${APP_DIR}/venv/bin/activate \
  && python -m pip \
  install \
  --upgrade \
  --no-cache-dir \
  --no-index \
  ${APP_DIR}/*.whl \
  && rm -rf ${APP_DIR}/*.whl

ENTRYPOINT [ "/entrypoint.sh" ]

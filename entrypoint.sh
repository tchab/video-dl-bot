#!/bin/sh

set -eu

# shellcheck source=/dev/null
. "${APP_DIR:-}"/venv/bin/activate
python -m video_dl_bot

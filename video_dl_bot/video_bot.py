#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Simple Bot to fetch video and upload them to telegram.
Usage:
Press Ctrl-C on the command line or send a signal to the process to stop the
bot.
"""
import asyncio
import datetime
import imghdr
import logging
import os
import re
import subprocess
import unicodedata
from difflib import SequenceMatcher

import configargparse as configargparse
import magic
import validators
import yt_dlp
from polyglot.detect import Detector
from polyglot.detect.base import UnknownLanguage
from telethon import TelegramClient, events

# Default download location
VIDEO_DL_DIR = "/tmp"

UPLOAD_TIMEOUT = 999

URL_REGEX = r"(?P<url>https?:\/\/[^\s]+)"

MAX_CAPTION_LENGTH = 150

BOT_MAX_FILE_SIZE = 52428800

# Enable logging
logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)

logger = logging.getLogger(__name__)

p = configargparse.ArgParser()
p.add(
    "-t",
    "--bot-token",
    required=True,
    help="Telegram Bot token. You can get one following instructions https://core.telegram.org/bots#6-botfather",
    env_var="VIDEO_DL_BOT_TOKEN",
)
p.add(
    "-i",
    "--api-id",
    required=True,
    help="Telegram API id. You can get one at https://my.telegram.org",
    env_var="TG_API_ID",
)
p.add(
    "-p",
    "--api-hash",
    required=True,
    help="Telegram API hash. You can get one at https://my.telegram.org",
    env_var="TG_API_HASH",
)
p.add(
    "-d",
    "--data-dir",
    required=False,
    help="Data directory used to store history and Telegram session",
    env_var="DATA_DIR",
)

config = p.parse_args()

# Use your own values from my.telegram.org
api_id = config.api_id
api_hash = config.api_hash

config.data_dir = config.data_dir or "/data"
bot = TelegramClient(f"{config.data_dir}/bot.session", api_id, api_hash).start(
    bot_token=config.bot_token
)
user = TelegramClient(f"{config.data_dir}/user.session", api_id, api_hash)


@bot.on(events.NewMessage(pattern="/help"))
async def show_help(event):
    """Send a message when the command /help is issued."""
    await event.respond(
        """*Available commands* : 
                                 - Download video : /dl [url]
                                 - Make gif from video : /gif [url]
                                 - Get mp3 from video : /mp3 [url]
                                 """
    )
    raise events.StopPropagation


def find_url(text):
    url_search = re.search(URL_REGEX, text)
    return url_search.group("url") if url_search is not None else text


@bot.on(events.NewMessage(pattern="/dl"))
async def download(event):
    logger.info(f'Download query - args: "{event.message}"')
    await download_url(event, find_url(event.text))


@bot.on(events.NewMessage(pattern="/dlt"))
async def download(event):
    logger.info(f'Download query - args: "{event.message}"')
    await download_url(event, find_url(event.text), caption=True)


@bot.on(events.NewMessage(pattern="/gif"))
async def gif(event):
    logger.info(f'Gif download query - args: "{event.message}"')
    await download_url(event, find_url(event.text), output_gif=True)


@bot.on(events.NewMessage(pattern="/mp3"))
async def mp3(event):
    logger.info(f'Mp3 download query - args: "{event.message}"')
    await download_url(event, find_url(event.text), audio_only=True)


def run_subprocess(cmd):
    process_result = subprocess.run(cmd, capture_output=True, check=True)
    logger.info(
        f'Ran command {cmd} and get following output:\n{process_result.stdout.decode("utf-8")}'
    )
    if process_result.returncode != 0:
        logger.error(f'Error running command: {process_result.stderr.decode("utf-8")}')


def get_length(filename):
    if os.path.isfile(filename):
        result = subprocess.run(
            [
                "ffprobe",
                "-v",
                "error",
                "-show_entries",
                "format=duration",
                "-of",
                "default=noprint_wrappers=1:nokey=1",
                filename,
            ],
            capture_output=True,
            check=True,
        )
        size = float(result.stdout)
    else:
        size = 0
    return size


def generate_thumb(video_file_name):
    thumb_file_name = os.path.splitext(video_file_name)[0] + ".jpg"
    if (
        os.path.isfile(video_file_name)
        and os.path.isfile(thumb_file_name)
        and imghdr.what(thumb_file_name) == "jpeg"
    ):
        real_thumb = thumb_file_name
    else:
        if os.path.isfile(thumb_file_name):
            os.remove(thumb_file_name)
        video_length = datetime.timedelta(
            seconds=float("{0:.2f}".format(get_length(video_file_name) / 2))
        )
        run_subprocess(
            [
                "ffmpeg",
                "-i",
                video_file_name,
                "-ss",
                str(video_length),
                "-vframes",
                "1",
                thumb_file_name,
            ]
        )
        real_thumb = thumb_file_name
    return real_thumb


def similar(first_string, last_string):
    return SequenceMatcher(None, first_string, last_string).ratio()


def normalize_caseless(text):
    return unicodedata.normalize(
        "NFKD", text.replace('"', "").replace("'", "").casefold()
    )


def get_file_name_from_metadata(data_dir, name):
    full_file_path = ""
    logger.info(f"Searching file name matching pattern {name}")
    for entry in os.scandir(data_dir):
        file_name = os.path.basename(entry)
        similarity_ratio = similar(
            normalize_caseless(os.path.splitext(file_name)[0]), normalize_caseless(name)
        )
        logger.info(
            f"Checking file {file_name} with pattern {name} : {similarity_ratio * 100}%"
        )
        if similarity_ratio > 0.8:
            full_file_path = os.path.abspath(entry)
            logger.info(
                f"{file_name} is a potential match for {name}. Checking mime type ..."
            )
            mime = magic.Magic(mime=True)
            mime_type = mime.from_file(full_file_path)
            logger.info(f"Mime for file {full_file_path} is {mime_type}")
            if mime_type.find("video") != -1:
                logger.info(f"{full_file_path} is a video")
                break
        else:
            logger.info(f"{file_name} and {name} don't match. Moving on ... ")

    return full_file_path


def transcode_file(file, is_gif, audio_only):
    base_name = os.path.splitext(file)[0]
    final_name = f"{base_name}.final.mp4"
    ffmpeg_cmd = [
        "ffmpeg",
        "-i",
        file,
        "-y",
        "-vcodec",
        "libx264",
        "-crf",
        "17",
        "-vprofile",
        "high",
        "-level",
        "4.0",
        "-refs",
        "1",
        "-threads",
        "0",
        "-pix_fmt",
        "yuv420p",
        "-codec:a",
        "aac",
        "-ac",
        "2",
        "-movflags",
        "+faststart",
        final_name,
    ]

    if is_gif:
        ffmpeg_cmd = [
            "ffmpeg",
            "-i",
            file,
            "-y",
            "-vcodec",
            "libx264",
            "-crf",
            "23",
            "-vprofile",
            "baseline",
            "-level",
            "3.0",
            "-refs",
            "1",
            "-b:v",
            "500k",
            "-maxrate",
            "500k",
            "-bufsize",
            "800k",
            "-vf",
            "scale=-2:480",
            "-threads",
            "0",
            "-pix_fmt",
            "yuv420p",
            "-an",
            "-movflags",
            "+faststart",
            final_name,
        ]
    elif audio_only:
        final_name = f"{base_name}.mp3"
        ffmpeg_cmd = [
            "ffmpeg",
            "-i",
            file,
            "-vn",
            "-q:a",
            "0",
            "-map",
            "a?",
            final_name,
        ]

    run_subprocess(ffmpeg_cmd)

    return final_name


def get_languages_counts(text):
    nb_langs = 0
    if len(text.split()) > 1:
        try:
            detector = Detector(text)
            nb_langs = (
                len(detector.languages)
                if (detector is not None) and (detector.languages is not None)
                else 0
            )
            logger.info(f'Found {nb_langs} languages in text "{text}"')
        except UnknownLanguage as e:
            logger.info(
                f'Failed to detect language for text {text} because of exception : "{e}"'
            )
    return nb_langs


def empty_str(text):
    return text if text and text.strip() and get_languages_counts(text) > 0 else ""


async def upload_file(telegram_chat, file, caption, output_gif, thumb_file):
    use_bot = (
        True
        if os.path.isfile(file) and os.stat(file).st_size < BOT_MAX_FILE_SIZE
        else False
    )
    if use_bot:
        await bot.send_file(
            telegram_chat.chat,
            file=file,
            caption=caption if not output_gif else "",
            thumb=thumb_file if os.path.isfile(thumb_file) else "",
            supports_streaming=True,
        )
    else:
        if os.path.isfile(file):
            loop = asyncio.get_event_loop()
            await loop.create_task(
                upload_large_file(caption, file, output_gif, telegram_chat, thumb_file)
            )


async def upload_large_file(caption, file, output_gif, telegram_chat, thumb_file):
    await user.connect()
    await user.get_me()
    logger.info(f"User object successfully initialized")
    logger.info(
        f"Max bot file size {BOT_MAX_FILE_SIZE} exceeded, sending file using user account."
    )
    await user.send_file(
        telegram_chat.chat,
        file=file,
        caption=caption if not output_gif else "",
        thumb=thumb_file if os.path.isfile(thumb_file) else "",
        supports_streaming=True,
    )
    logger.info(f"File {file} uploaded successfully.")
    await user.disconnect()
    logger.info(f"User disconnected successfully.")
    return user


async def download_url(
    telegram_chat,
    url,
    output_gif=False,
    audio_only=False,
    with_proxy=False,
    caption=False,
):
    if not validators.url(url):
        await telegram_chat.reply("Please, stop sending shit.")
        return
    if with_proxy:
        logger.error("Proxy feature is now deprecated")
    try:
        ydl_opts = ytdl_config(telegram_chat.chat_id)
        with yt_dlp.YoutubeDL(ydl_opts) as ydl:
            logger.info('Downloading video from "%s"', url)
            metadata = ydl.extract_info(url, download=True)
            logger.info(
                'Video successfully downloaded to "%s"',
                f'{VIDEO_DL_DIR}/{telegram_chat.chat_id}/{metadata.get("id")}',
            )
            downloaded_name = get_file_name_from_metadata(
                f"{VIDEO_DL_DIR}/{telegram_chat.chat_id}", metadata.get("id")
            )
            if os.path.isfile(downloaded_name):
                final_name = transcode_file(downloaded_name, output_gif, audio_only)
                thumb_file_name = generate_thumb(downloaded_name)
                description_labels = [
                    empty_str(metadata.get("title")),
                    empty_str(metadata.get("description")),
                ]
                description = " : ".join(
                    filter(None, description_labels)
                )  # add ' : ' separator only if both strings are not empty
                file_caption = (
                    f"{description[:MAX_CAPTION_LENGTH - 3]}..."
                    if len(description) > MAX_CAPTION_LENGTH
                    else description
                )  # add ... if description is too long

                # if len(metadata.get('title').strip()) > 0:  # To avoid shitty filenames
                #     formatted_name = f'{metadata.get("title")}.{os.path.splitext(final_name)[1]}'
                #     shutil.move(final_name, formatted_name)

                await upload_file(
                    telegram_chat,
                    final_name,
                    file_caption if caption else "",
                    output_gif,
                    thumb_file_name,
                )

                # clean(formatted_name)
                clean(downloaded_name)
                clean(thumb_file_name)
                clean(final_name)  # To handle case where yt-dlp merge in mkv file
            else:
                logger.fatal(f"No file found matching video : {metadata.get('id')}")
    except Exception as e:
        logger.fatal(f"Download failed: {e}")
        await telegram_chat.reply(f"Sorry, download failed due to error : {str(e)}")


def ytdl_config(chat_id):
    ydl_opts = {
        "outtmpl": f"{VIDEO_DL_DIR}/{chat_id}/%(id)s.%(ext)s",
        "addmetadata": True,
        "xattrs": True,
        "geo_bypass": True,
        "continuedl": True,
        "fragment_retries": 10,
        "noplaylist": True,
        "verbose": True,
    }

    return ydl_opts


def clean(file_name):
    if os.path.isfile(file_name):
        try:
            os.remove(file_name)
        except OSError as e:
            logger.error('Error "%s" removing file "%s"', e, file_name)
    else:
        logger.info(f"Not removing {file_name} : file not found")


def error(update, context):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, context.error)


def main():
    bot.run_until_disconnected()


if __name__ == "__main__":
    main()
